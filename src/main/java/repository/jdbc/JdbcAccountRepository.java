package repository.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import repository.AccountRepository;
import domain.Account;

public class JdbcAccountRepository implements AccountRepository {
	public Account save(Account account) { 
		Connection con = null;
        PreparedStatement pst = null;

        String url = "jdbc:postgresql://localhost/bank";
        String user = "postgres";
        String password = "postgres";

        try {
            con = DriverManager.getConnection(url, user, password);
            String stm = "INSERT INTO account(number, balance) VALUES(?, ?)";
            boolean update = findByNumber(account.getNumber()) != null;
            if (update) {
            	stm = "UPDATE account SET number = ?, balance = ? WHERE number = ?";
            }
            pst = con.prepareStatement(stm);
            pst.setString(1, account.getNumber());
            pst.setDouble(2, account.getBalance());
            if (update) {
                pst.setString(3, account.getNumber());
            }
            pst.executeUpdate();
        } catch (SQLException ex) {
        	ex.printStackTrace();
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
            	ex.printStackTrace();
            }
        }
        return account;
    }

	@Override
	public Account findByNumber(String number) {
		Connection con = null;
        PreparedStatement pst = null;

        String url = "jdbc:postgresql://localhost/bank";
        String user = "postgres";
        String password = "postgres";

        try {
            con = DriverManager.getConnection(url, user, password);
            String stm = "SELECT number, balance FROM account WHERE number = '" + number + "'";
            pst = con.prepareStatement(stm);
            pst.setString(1, number);
            pst.execute();
            ResultSet rs = pst.getResultSet();
            if (rs.next()) {
            	Account account = new Account();
            	account.setNumber(rs.getString(1));
            	account.setBalance(rs.getDouble(2));
            }
            return null;
        } catch (SQLException ex) {
        	ex.printStackTrace();
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
            	ex.printStackTrace();
            }
        }
        return null;
	}

	@Override
	public List<Account> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Account remove(Account account) {
		// TODO Auto-generated method stub
		return null;
	}
}