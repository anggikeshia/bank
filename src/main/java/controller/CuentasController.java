package controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
//import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import service.TransferService;
import service.cuentaService;
import repository.AccountRepository;
import repository.InMemoryAccountRepository;

@Controller

public class CuentasController {
public AccountRepository repository;
public TransferService trans;
public cuentaService aservice;
public CuentasController(){

	repository= new InMemoryAccountRepository();
	trans = new TransferService(repository);
	aservice = new cuentaService(repository);
}


@RequestMapping("/home")

String Home() {
	return "Transferencia";}

@RequestMapping("/transferencia")
String transferencia(@RequestParam String c1, @RequestParam String c2, @RequestParam String monto) {
	double m = Double.parseDouble(monto);
	boolean res =trans.transfer(c1, c2, m);
	if(res == true)
		{
		return "Acceso";
		}
	return "Error";
	}
@RequestMapping("/Lista")
	String Usuarios(ModelMap model) {
	model.addAttribute("Lista",aservice.getlist());
	return "Lista";
	}
}
